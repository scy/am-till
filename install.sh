#!/bin/sh
set -e

mkdir -p css

(
	cd css
	curl -O https://raw.githubusercontent.com/picocss/pico/master/css/pico.min.css{,.map}
)
